package stepDefinition;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cucumber.listener.Reporter;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dataProvider.ConfigFileReader;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Test_Steps {

	private static WebDriver driver = null;
	ConfigFileReader configFileReader= new ConfigFileReader();
	ReadDataFromExcel data=new ReadDataFromExcel();
	Scenario scenario;
	
	@Before
	public void before(Scenario scenario) {
	    this.scenario = scenario;
	}
	
	@Given("^User is on Home Page$")
	public void User_is_on_Home_Page() throws Throwable {
		// Express the Regexp above with the code you wish you had
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(configFileReader.getApplicationUrl());
	}

//	@When("^User enters \"(.*)\" and \"(.*)\"$")
//	public void User_enters_UserName_and_Password(String userName,String password) throws Throwable {
//		driver.findElement(By.name("userName")).sendKeys(userName);
//		driver.findElement(By.name("password")).sendKeys(password);
//		driver.findElement(By.name("login")).click();
//	}
//Passing data as datatable	
//	@When("^User enters credentials to login$")
//	public void User_enters_UserName_and_Password(DataTable usercredentials) throws Throwable {
//		//Write the code to handle Data Table
//		 List<List<String>> data = usercredentials.raw();
//		 
//		 //This is to get the first data of the set (First Row + First Column)
//		 driver.findElement(By.name("userName")).sendKeys(data.get(0).get(0)); 
//		 
//		 //This is to get the first data of the set (First Row + Second Column)
//		 driver.findElement(By.name("password")).sendKeys(data.get(0).get(1));
//	 
//		 driver.findElement(By.name("login")).click();
//	}
//Passing data using MAPS	
/*	@When("^User enters credentials to login$")
	public void User_enters_UserName_and_Password(DataTable usercredentials) throws Throwable {
		//Write the code to handle Data Table
		 List<Map<String,String>> data = usercredentials.asMaps(String.class,String.class);
		 
		 //This is to get the first data of the set (First Row + First Column)
		 driver.findElement(By.name("userName")).sendKeys(data.get(0).get("userName")); 
		 
		 //This is to get the first data of the set (First Row + Second Column)
		 driver.findElement(By.name("password")).sendKeys(data.get(0).get("password"));
	 
		 driver.findElement(By.name("login")).click();
	}
*/
	//Passing multiple data using MAps
	
/*	@When("^User enters credentials to login$")
	public void User_enters_UserName_and_Password(DataTable usercredentials) throws Throwable {
		for (Map<String,String>data:usercredentials.asMaps(String.class,String.class)){
		 //This is to get the first data of the set (First Row + First Column)
		driver.findElement(By.name("userName")).clear();
		 driver.findElement(By.name("userName")).sendKeys(data.get("userName")); 
		 
		 //This is to get the first data of the set (First Row + Second Column)
		 driver.findElement(By.name("password")).clear();
		 driver.findElement(By.name("password")).sendKeys(data.get("password"));
		 
	//	 
		}
	}
*/	
//	Map Data Tables to Class Objects
	
	/*@When("^User enters credentials to login$")
	public void User_enters_UserName_and_Password(List<dataParameters> usercredentials) throws Throwable {
		for (dataParameters credentials : usercredentials){
		 //This is to get the first data of the set (First Row + First Column)
		 driver.findElement(By.name("userName")).clear();
		 driver.findElement(By.name("userName")).sendKeys(credentials.getuserName()); 
		 
		 //This is to get the first data of the set (First Row + Second Column)
		 driver.findElement(By.name("password")).clear();
		 driver.findElement(By.name("password")).sendKeys(credentials.getpassword());		 
	//	 
		}
	}*/
	
	//Using excel data
	@When("^User enters \"(.*)\" and \"(.*)\"$")
	public void User_enters_UserName_and_Password(String arg1,String arg2) throws Throwable {
		 //This is to get the first data of the set (First Row + First Column)
		 driver.findElement(By.name("userName")).clear();
		 System.out.println(data.get(scenario.getName(), arg1));
		 driver.findElement(By.name("userName")).sendKeys(data.get(scenario.getName(), arg1)); 
		 
		 //This is to get the first data of the set (First Row + Second Column)
		 driver.findElement(By.name("password")).clear();
		 driver.findElement(By.name("password")).sendKeys(data.get(scenario.getName(), arg2));		 
	//	 
		}
	@Then("^Message displayed Login Successfully$")
	public void Message_displayed_Login_Successfully() throws Throwable {
		driver.findElement(By.name("login")).click();
		System.out.println("Login Successfully");
	}

	@When("^User LogOut from the Application$")
	public void User_LogOut_from_the_Application() throws Throwable {
		driver.findElement(By.linkText("SIGN-OFF")).click();
	}

	@Then("^Message displayed LogOut Successfully$")
	public void Message_displayed_LogOut_Successfully() throws Throwable {
		System.out.println("LogOut Successfully");
		driver.quit();
	}
	

}


